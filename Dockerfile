FROM phusion/baseimage:0.9.19

RUN apt-get update \
    && sh -c "curl -sSL https://get.docker.com/ | sh" \
    && sh -c "curl -L https://packages.gitlab.com/install/repositories/runner/gitlab-ci-multi-runner/script.deb.sh | bash" \
    && apt-get install -y gitlab-ci-multi-runner \
    && apt-get clean && rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/*

CMD ["/sbin/my_init"]
